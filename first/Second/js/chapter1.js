﻿$(function () {
    var pageIndex = 0;
    var width = $('#container').width();
    var count = $('.sections').children().length;
    var fixPageIndex = 5;
    var movepoint = {
        startX: 0,
        startY: 0,
        middleX: 0,
        middleY: 0,
        endX: 0,
        endY: 0,
        startLeft: 0,
        isChange: false//用来判断有没有改变
    };
    var computeY = ComputeMoveY();
    var musiclist = { openlight: 'openlight.mp3', last: 'last.mp3', firstbroke: 'eggbroke.mp3', secondbroke: 'eggbroke.mp3', wait: 'wait.mp3' };//只有第一次有播放音效和其它行为
    var duration = 500;//动画延迟

    function layout() {
        $('.sections').width(width * count);
        $('.sections').children().width(width);
        for (var i = 0; i < $('.sections').children().length; i++) {
            (function (a) {
                $('.sections').children().eq(a).css('left', -1 * a * width);
                if (a !== 0) {
                    $('.sections').children().eq(a).css('-webkit-transform', 'translate3d(' + width + 'px,0px,0px)');
                }
                else {
                    $('.sections').children().eq(a).css('-webkit-transform', 'translate3d(0px,0px,0px)');
                }
            })(i);
        }
    }

    //直接根据触摸点计算出点击的pageindex
    function getPageIndex() {
        var index = 0;
        for (var i = 0; i < $('.sections').children().length; i++) {
            var me = $('.sections').children().eq(i);
            if (me.position().left < movepoint.startX && (me.position().left + width) > movepoint.startX) {
                index = i;
                break;
            }
        }
        return index;
    }

    //dis:左右移动的距离
    function move(dis) {
        var index = pageIndex;
        //
        movepoint.isChange = true;
        if (index === fixPageIndex) {
            computeY();
            return;
        }
        //最左边和最右边都不能移动,还有固定page
        if ((dis > 0 && pageIndex === 0) || (dis < 0 && pageIndex === count - 1)) {
            return;
        }
        //下面才是可以移动
        dis += movepoint.startLeft;
        if (index !== 0) {
            $('.sections').children().eq(index - 1).css('-webkit-transform', 'translate3d(' + (dis - width) + 'px,0px,0px)').css('-webkit-transition-duration', '0ms');
        }
        $('.sections').children().eq(index).css('-webkit-transform', 'translate3d(' + dis + 'px,0px,0px)').css('-webkit-transition-duration', '0ms');
        if (index !== count - 1) {
            $('.sections').children().eq(index + 1).css('-webkit-transform', 'translate3d(' + (dis + width) + 'px,0px,0px)').css('-webkit-transition-duration', '0ms');
        }
    }

    //计算上下移动的距离，并且根据距离作出相应动作
    function ComputeMoveY() {
        //
        var prevMovePoint = { middleY: 0 };
        var measureY = new MeasureDis();
        function MeasureDis() {
            var distance = 0;
            return function (dis) {
                return distance += dis;
            }
        };

        return function () {
            var chazhiY = 0;
            //计算移动的距离
            if (prevMovePoint.middleY === 0) {
                chazhiY = Math.abs(movepoint.middleY - movepoint.startY);
            }
            else {
                chazhiY = Math.abs(movepoint.middleY - prevMovePoint.middleY);
                console.log(movepoint.middleY + "    " + prevMovePoint.middleY);
            }

            prevMovePoint.middleY = movepoint.middleY;
            var result = measureY(chazhiY);

            var targets = [2000, 6000, 8000];

            //添加是否3秒没有上下触摸的判断
            if (result > targets[1] && musiclist.secondbroke) {
                $('#img8').show();
                $('#img7').hide();
                playMusic(musiclist.secondbroke);
                delete musiclist.secondbroke;
            } else if (result > targets[0] && musiclist.firstbroke) {
                $('#img7').show();
                $('#img6').hide();
                playMusic(musiclist.firstbroke);
                delete musiclist.firstbroke;
            }
            if (result > targets[2] && musiclist.wait) {
                $('.flash').hide();
                $('#img8').hide();
                $('#wait').show();
                $('#img89').show();
                playMusic(musiclist.wait);
                delete musiclist.wait;

                //等等等的画面
                setTimeout(function () {
                    //最后的图出现
                    if (musiclist.last) {
                        playMusic(musiclist.last);
                    }
                    $('#wait').hide();
                    $('#img89').hide();
                    $('#img9').show();
                    setTimeout(function () {
                        fixPageIndex = 1000;
                    }, 500)
                }, 4000);
            }
        };
    };

    //滚动page
    function scrollPage(index, callback) {
        if (index !== 0) {
            $('.sections').children().eq(index - 1).css('-webkit-transform', 'translate3d(' + (-width) + 'px,0px,0px)').css('-webkit-transition-duration', duration + "ms");
        }
        $('.sections').children().eq(index).css('-webkit-transform', 'translate3d(' + 0 + 'px,0px,0px)').css('-webkit-transition-duration', duration + "ms");
        if (index !== count - 1) {
            $('.sections').children().eq(index + 1).css('-webkit-transform', 'translate3d(' + width + 'px,0px,0px)').css('-webkit-transition-duration', duration + "ms");
        }

        //防止bug,太快速移动会有不能移动的问题
        for (var i = 0; i < count; i++) {
            if (i < index - 1) {
                $('.sections').children().eq(i).css('-webkit-transform', 'translate3d(' + (-width) + 'px,0px,0px)').css('-webkit-transition-duration', '0ms');
            }
            else if (i > index + 1) {
                $('.sections').children().eq(i).css('-webkit-transform', 'translate3d(' + width + 'px,0px,0px)').css('-webkit-transition-duration', '0ms');
            }
        }
        //回调函数
        setTimeout(function () {
            if (callback && typeof callback === 'function') {
                callback();
            }
        }, duration);
    }

    function playMusic(str) {
        //音乐播放
        var musicctrl = $('#effectMusic');
        musicctrl.attr('src', 'music/' + str);
        musicctrl[0].play();
    }

    //根据起点和终点返回方向 1：向上，2：向下，3：向左，4：向右,0：未滑动
    var GetSlideDirection = function (startX, startY, endX, endY) {
        var dy = startY - endY;
        var dx = endX - startX;
        var result = 0;

        //如果滑动距离太短
        if (Math.abs(dx) < 2 && Math.abs(dy) < 2) {
            return result;
        }

        var angle = GetSlideAngle(dx, dy);
        if (angle >= -45 && angle < 45) {
            result = 4;
        } else if (angle >= 45 && angle < 135) {
            result = 1;
        } else if (angle >= -135 && angle < -45) {
            result = 2;
        }
        else if ((angle >= 135 && angle <= 180) || (angle >= -180 && angle < -135)) {
            result = 3;
        }
        return result;
    }

    //返回角度
    function GetSlideAngle(dx, dy) {
        return Math.atan2(dy, dx) * 180 / Math.PI;
    }

    layout();


    $('#container').on('touchstart', function (e) {
        movepoint.startX = e.originalEvent.touches[0].pageX;
        movepoint.startY = e.originalEvent.touches[0].pageY;
        movepoint.startLeft = parseInt($('.sections').children().eq(pageIndex).css('-webkit-transform').split(',')[4]);
        pageIndex = getPageIndex();
        move(0);

    }).on('touchmove', function (e) {
        e.preventDefault();
        movepoint.middleX = e.originalEvent.targetTouches[0].pageX;
        movepoint.middleY = e.originalEvent.targetTouches[0].pageY;
        var dis = movepoint.middleX - movepoint.startX;
        move(dis);
        e.stopPropagation();
    }).on('touchend', function (e) {
        e.preventDefault();
        movepoint.endX = e.originalEvent.changedTouches[0].pageX;
        movepoint.endY = e.originalEvent.changedTouches[0].pageY;
        //不能移动的page
        if (fixPageIndex === pageIndex)
            return;
        var direction = GetSlideDirection(movepoint.startX, movepoint.startY, movepoint.endX, movepoint.endY);
        if (Math.abs(movepoint.startX - movepoint.endX) > 0.2 * width) {
            switch (direction) {
                //case 0:
                //    //alert("没滑动");
                //    break;
                //case 1:
                //    //alert("向上");
                //    break;
                //case 2:
                //    //alert("向下");/
                //    break;
                case 3:
                    if (pageIndex < $('.sections').children().length - 1) {
                        pageIndex++;
                    }
                    break;
                case 4:
                    if (pageIndex > 0) {
                        pageIndex--;
                    }
                    break;
                default:
            }
        }
        setPageEvent(pageIndex);
    });

    //滚动事件的中间函数，可以添加回调函数
    function setPageEvent(index) {
        //最后一页canvas要去掉最前显示
        if (index === count - 1) {
            $('#myCanvas').css('z-index', '-1');
        }
        else {
            $('#myCanvas').css('z-index', '1000');
        }

        if (index === 3 && musiclist.openlight) {
            scrollPage(index, function () {
                playMusic(musiclist.openlight);
                delete musiclist.openlight;
            });
        }
        else if (index === 5 && index === fixPageIndex) {
            scrollPage(index, function () {
                //先启动手的动画2s，然后开始监测是否有上下滑动
                $('.hand-img').css('display', 'block');
                setTimeout(function () {
                    $('.hand-img').hide();
                }, 2000);

                //3s没有滑动,则开始提醒
                function warnShow() {
                    var time = 1500;
                    $('#warn').show();
                    $('.hand-img').css('display', 'block').css('animation-play-state', 'running').css('-webkit-animation-play-state', 'running');
                    setTimeout(function () {
                        $('.hand-img').css('display', 'none').css('animation-play-state', 'paused')
    .css('-webkit-animation-play-state', 'paused');
                        $('#warn').hide();
                    }, time);
                }
                var testMove = setInterval(function () {
                    if (movepoint.isChange) {
                        movepoint.isChange = false;
                    }
                    else {
                        warnShow();
                    }
                    if ($('#img6').css('display') === 'none' && $('#img7').css('display') === 'none' && $('#img8').css('display') === 'none') {
                        clearInterval(testMove);
                    }
                }, 3000);
            });
        }
        else {
            scrollPage(index);
        }
    }
});